// Restify API Server setup:
var restify = require('restify')
var server = restify.createServer()

server.use(restify.fullResponse())
server.use(restify.queryParser())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())

restify.CORS.ALLOW_HEADERS.push('authorization')
restify.CORS.ALLOW_HEADERS.push('owner')

var getUsers = require('./getUsers.js')
var getPosts = require('./getPosts.js')
var getComments = require('./getComments.js')

var port = process.env.PORT || 8080
server.listen(port, err => {
    if (err) {
        console.error(err)
    } else {
        console.log('API is ready at port: '+port)
    }
})

// Here the three required API endpoints are created.
server.get('/users', (req, res) => {
    console.log('GET from /users.')
    const auth = req.authorization
    getUsers.getUsers(auth, data => {
        if (data.code == '200') {
            console.log('Returning all users.')
        } else { 
            console.log(data.response.message) 
        }
        res.send(data.code, data.response)
        res.end()
    })
})

server.get('/posts', (req, res) => {
    console.log('GET from /posts.')
    const auth = req.authorization
    const userId = req.params.userId
    getPosts.getPosts(auth, userId, data => {
        if (data.code == '200') {
            console.log('Returning all posts userId: '+userId)
        } else { 
            console.log(data.response.message) 
        }
        res.send(data.code, data.response)
        res.end()
    })
})

server.get('/comments', (req, res) => {
    console.log('GET from /comments.')
    const auth = req.authorization
    const postId = req.params.postId
    getComments.getComments(auth, postId, (data) => {
        if (data.code == '200') {
            console.log('Returning all comments for postId: '+postId)
        } else { 
            console.log(data.response.message) 
        }
        res.send(data.code, data.response)
        res.end()
    })
})