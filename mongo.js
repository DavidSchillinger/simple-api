var mongoose = require('mongoose')
//var uuid = require('node-uuid') This module could be used in order to generate unique user/post/comment IDs.

// Database setup.
var database = 'api'

const server = 'mongodb://'+process.env.IP+'/'+database
console.log(server)

// Mongoose connection.
mongoose.connect(server)
const db = mongoose.connection

// Schema for the USERS
const usersSchema =
new mongoose.Schema({
    id: { type: String },
    name: { type: String },
    username: { type: String },
    email: { type: String }
})

const Users = mongoose.model('Users', usersSchema)

// Schema for the POSTS
const postsSchema =
new mongoose.Schema({
    id: { type: String },
    userId: { type: String },
    title: { type: String },
    message: { type: String }
})

const Posts = mongoose.model('Posts', postsSchema)

// Schema for the COMMENTS
const commentsSchema =
new mongoose.Schema({
    id: { type: String },
    postId: { type: String },
    message: { type: String }
})

const Comments = mongoose.model('Comments', commentsSchema)

// END OF MONGOOSE SETUP

// This function retrieves users from the database.
exports.getUsers = callback => {
    Users.find((err, data) => {
        if (err) {
            callback('error: '+err)
        } else if (data[0] != undefined) {
            var users = data.map(item => {
                return {id: item.id, name: item.name, username: item.username, email: item.email}
            })
            callback({code: 200, response: users})
        } else {
            callback({code: 404, response: {message: 'No Users found.'}})
        }
    })
}

// This one retrieves the posts for a specified user by userId
exports.getPosts = (userId, callback) => {
    Posts.find({userId: userId}, (err, data) => {
        if (err) {
            callback('error: '+err)
        } else if (data[0] != undefined) {
            var posts = data.map(item => {
                return {id: item.id, userId: item.userId, title: item.title, message: item.message}
            })
            callback({code: 200, response: posts})
        } else {
            callback({code: 404, response: {message: 'No posts found for this userId: '+userId}})
        }
    })
}

// And this one retrieves the comments for a specified post by postId
exports.getComments = (postId, callback) => {
    Comments.find({postId: postId}, (err, data) => {
        if (err) {
            callback('error: '+err)
        } else if (data[0] != undefined) {
            var comments = data.map(item => {
                return {id: item.id, postId: item.postId, message: item.message}
            })
            callback({code: 200, response: comments})
        } else {
            callback({code: 404, response: {message: 'No comments found for this postId: '+postId}})
        }
    })
}