var mongo = require('./mongo.js')

// Authorisation test.
function authorisationSent(auth) {
	console.log('Checking authentication.')
	return new Promise((resolve, reject) => {
		console.log('  a')
		if (auth.scheme !== 'Basic') {
			console.log('  b')
			console.log('Basic authentication missing.')
			return reject({code: 401, response:{status:'error', message:'Basic access authentication required.'}})
		}
		if (auth.basic.username === 'simple' || auth.basic.password === 'webdev') {
			console.log('  c')
			console.log('Access granted. Correct username and password.')
			return resolve({code: 200, response:{ status:'success', message:'Access granted.'}})
		}
		console.log('  d')
		return reject({code: 401, response:{status:'error', message:'Access denied. Wrong username and/or password.'}})
	})
}

// Search for database entries.
function search() {
	console.log('Looking up user(s).')
	return new Promise((resolve, reject) => {
		console.log('  a')
		mongo.getUsers(mongoReturns => {
			mongoReturns.contentType = 'application/json'
			return resolve(mongoReturns)
		})
	})
}

// Promise chain.
exports.getUsers = (auth, callback) => {
	console.log('---1---')
	authorisationSent(auth)
	.then(() => {
		console.log('---2---')
		return search()
	}).then(data => {
		console.log('---3---')
		callback(data)
	}).catch(data => {
		console.log('---ERROR---')
		console.log('MAIN CATCH')
		callback(data)
	})
}